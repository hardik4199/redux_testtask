import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="bg-secondary navbar shadow fixed-top navbar-expand-sm navbar-dark ">
      <div className="container">
        <Link to="/" className="navbar-brand">
          Home
        </Link>
        <div>
          <Link to="contact/add" className="btn btn-light ml-auto">
            Add Item
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
