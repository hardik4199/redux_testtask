import { combineReducers } from "redux";
import itemReducer from "./Reducer";

const rootReducer = combineReducers({
  item: itemReducer,
});

export default rootReducer;
