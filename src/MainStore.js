import { createStore } from "redux";
import rootReducer from "./redux/reducer/index";

export const Mainstore = createStore(rootReducer);
